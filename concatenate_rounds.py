import pandas as pd

players_1_df = pd.read_csv('players_1.csv', sep=',')
players_2_df = pd.read_csv('players_2.csv', sep=',')
players_3_df = pd.read_csv('players_3.csv', sep=',')
players_4_df = pd.read_csv('players_4.csv', sep=',')
players_5_df = pd.read_csv('players_5.csv', sep=',')
players_6_df = pd.read_csv('players_6.csv', sep=',')
players_7_df = pd.read_csv('players_7.csv', sep=',')
players_8_df = pd.read_csv('players_8.csv', sep=',')
players_9_df = pd.read_csv('players_9.csv', sep=',')
players_10_df = pd.read_csv('players_10.csv', sep=',')
players_11_df = pd.read_csv('players_11.csv', sep=',')

players_df = pd.concat([players_1_df, players_2_df, players_3_df, players_4_df, players_5_df, players_6_df,
                        players_7_df, players_8_df, players_9_df, players_10_df, players_11_df])


players_df.to_csv('players.csv', index=False, header=True)

print('')