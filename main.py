import csv

from omegaconf import OmegaConf

from utils import get_request, get_match_players

conf = OmegaConf.load('conf.yml')

players = []

hardcoded_matchday = 9  # remove it

for matchday in range(1, conf.webscraping.rounds + 1):
    matchday = hardcoded_matchday
    init_url = (f'{conf.webscraping.root_url}{conf.webscraping.init_url}'
                f'cod_primaria={conf.webscraping.cod_primaria}&'
                f'CodCompeticion={conf.webscraping.CodCompeticion}&'
                f'CodGrupo={conf.webscraping.CodGrupo}&'
                f'CodTemporada={conf.webscraping.CodTemporada}&'
                f'CodJornada={matchday}&'
                f'Sch_Codigo_Delegacion=&'
                f'ch_Tipo_Juego=')

    round_matches = []

    round_page = get_request(init_url)
    matches = round_page.find('table', {'class': 'table table-striped table-bordered table-hover table-light'}).findAll('table')
    for match in matches:
        round_matches.append(
            {
                'round': matchday,
                'home_team': match.find('div', {'class': 'font_widgetL'}).text.strip(' \t\r\n\xc2\xa0'),
                'away_team': match.find('div', {'class': 'font_widgetV'}).text.strip(' \t\r\n\xc2\xa0'),
                'result': None,
                'home_team_img_url': f'{conf.webscraping.root_url}'
                                     f'{match.find("div", {"class": "escudo_widgetL"}).find("img")["src"]}',
                'away_team_img_url': f'{conf.webscraping.root_url}'
                                     f'{match.find("div", {"class": "escudo_widgetV"}).find("img")["src"]}',
                'report_url': f'{conf.webscraping.root_url}'
                              f'{match.find("a", {"class": "btn green-meadow btn-sm"})["href"]}',
                'date': match.findAll('span', {'class': 'horario'})[0].text.strip(' \t\r\n\xc2\xa0'),
                'time': match.findAll('span', {'class': 'horario'})[1].text.strip(' \t\r\n\xc2\xa0'),
                'location': match.find('span', {'class': 'font_widgetL'}).contents[0].text.strip(' \t\r\n\xc2\xa0'),
                'referee': match.find('span', {'class': 'font_widgetL'}).contents[2].contents[2].text.strip(' \t\r\n\xc2\xa0')
            }
        )

    for match in round_matches:
        match_page = get_request(match['report_url'])

        match_players = []
        match_players += get_match_players(match, match_page, True, True)
        match_players += get_match_players(match, match_page, True, False)
        match_players += get_match_players(match, match_page, False, True)
        match_players += get_match_players(match, match_page, False, False)

        substitutions = match_page.findAll('table', {'class': 'table table-striped table-hover', 'width': None})

        for substitution in substitutions:
            subst_players = substitution.findAll('td')
            player_in = subst_players[1].text
            player_out = subst_players[4].text.split('\')')[1].strip(' \t\r\n\xc2\xa0')
            minute = subst_players[4].text.split('\')')[0].strip(' \t\r\n\xc2\xa0(')
            for player in match_players:
                if player['name'] == player_in:
                    player['minutes'] = 90 - int(minute)
                elif player['name'] == player_out:
                    player['minutes'] = int(minute)

        cards_home = match_page.findAll('table', {'class': 'table table-striped table-hover', 'width': '100%'})[2].findAll('tr')
        cards_away = match_page.findAll('table', {'class': 'table table-striped table-hover', 'width': '100%'})[5].findAll('tr')
        cards = cards_home + cards_away
        for card in cards:
            player_card = card.findAll('td')[1].text.split('\')')[1].strip(' \t\r\n\xc2\xa0')
            minute = card.findAll('td')[1].text.split('\')')[0].strip(' \t\r\n\xc2\xa0(')
            if 'tarj_amar' in card.findAll('td')[0].find('img')['src']:
                card_type = 'ta'
            else:
                card_type = 'tr'
            for player in match_players:
                if player['name'] == player_card:
                    if card_type == 'ta':
                        if player['ta1'] is None:
                            player['ta1'] = int(minute)
                        else:
                            player['ta2'] = int(minute)
                    elif card_type == 'tr':
                        player['tr'] = int(minute)
                        player['minutes'] = int(minute)

        goals = match_page.find(lambda tag: tag.name == 'table'
                                    and tag.get('class') == ['table']
                                    and tag.get('style') is None).findAll('tr')
        for goal in goals:
            goal_type = goal.find('a')['title'].strip(' \t\r\n\xc2\xa0')
            scorer = goal.find('td', {'class': 'font_responsive'}).text.split('\')')[1].strip(' \t\r\n\xc2\xa0')
            minute = int(goal.find('td', {'class': 'font_responsive'}).text.split('\')')[0].strip(' \t\r\n\xc2\xa0('))

            for player in match_players:
                if player['name'] == scorer:
                    if goal_type == 'Gol normal':
                        player['goals_normal'] += 1
                    elif goal_type == 'Gol de penalti':
                        player['goals_penalti'] += 1
                    elif goal_type == 'Gol en propia puerta':
                        player['goals_own'] += 1
                    else:
                        player['goals_other'] += 1
                    if minute <= 15:
                        player['goals_0'] += 1
                    elif 15 < minute <= 30:
                        player['goals_15'] += 1
                    elif 30 < minute <= 45:
                        player['goals_30'] += 1
                    elif 45 < minute <= 60:
                        player['goals_45'] += 1
                    elif 60 < minute <= 75:
                        player['goals_60'] += 1
                    elif 75 < minute:
                        player['goals_75'] += 1

        players += match_players
    break

with open(f'players_{hardcoded_matchday}.csv', 'w', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=players[0].keys())
    writer.writeheader()
    writer.writerows(players)
