import requests
import urllib3
import time

from fake_useragent import UserAgent
from bs4 import BeautifulSoup
from stem import Signal
from stem.control import Controller

urllib3.disable_warnings()

proxies = {
    'http': 'socks5://127.0.0.1:9050',
    'https': 'socks5://127.0.0.1:9050'
}


def get_request(url):
    headers = {'User-Agent': UserAgent().random}
    time.sleep(15)
    try:
        with Controller.from_port(port=9051) as c:
            c.authenticate(password='juanlzda')
            c.signal(Signal.NEWNYM)
            print(f"Your IP is : {requests.get('https://ident.me', proxies=proxies, headers=headers).text}")
            print(url)
            response = requests.get(url, headers=headers, proxies=proxies, verify=False)
    except urllib3.exceptions.InvalidChunkLength:
        print(f'Exception: {url}')
    if response is not None:
        return BeautifulSoup(response.content, 'html.parser')
    else:
        return None


def get_match_players(match, match_page, home, init):
    players_tables = match_page.findAll('table', {'class': 'table table-striped table-hover', 'width': '100%'})

    if home:
        home_away = 'home_team'
        if init:
            initial_team = True
            players = players_tables[0].findAll('tr')
        else:
            initial_team = False
            players = players_tables[1].findAll('tr')
    else:
        home_away = 'away_team'
        if init:
            initial_team = True
            players = players_tables[3].findAll('tr')
        else:
            initial_team = False
            players = players_tables[4].findAll('tr')

    players_data = []
    for player in players:
        players_data.append(
            {
                'name': player.findAll('td')[2].text.strip(' \t\r\n\xc2\xa0'),
                'number': player.findAll('td')[0].text.strip(' \t\r\n\xc2\xa0'),
                'image_base64': player.findAll('td')[1].find('img')['src'],
                'initial_team': initial_team,
                'round': match['round'],
                'team_name': match[home_away],
                'home_team': match['home_team'],
                'away_team': match['away_team'],
                'result': match['result'],
                'home_team_img_url': match['home_team_img_url'],
                'away_team_img_url': match['away_team_img_url'],
                'date': match['date'],
                'time': match['time'],
                'location': match['location'],
                'referee': match['referee'],
                'minutes': 90,
                'ta1': None,
                'ta2': None,
                'tr': None,
                'goals_normal': 0,
                'goals_penalti': 0,
                'goals_own': 0,
                'goals_other': 0,
                'goals_0': 0,
                'goals_15': 0,
                'goals_30': 0,
                'goals_45': 0,
                'goals_60': 0,
                'goals_75': 0
            }
        )

    return players_data
