import pandas as pd

players_df = pd.read_csv('players.csv', sep=',')
results_df = pd.read_csv('results.csv', sep=',')
db_df = pd.merge(players_df, results_df, on=['home_team', 'away_team'])
db_df = db_df.drop(columns=['result_x'])
db_df = db_df.rename(columns={"result_y": "result"})

db_df.to_csv('db.csv', index=False)

print('')