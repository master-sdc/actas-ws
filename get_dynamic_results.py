import csv

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromeService
from omegaconf import OmegaConf
from bs4 import BeautifulSoup

PROXY = "socks5://127.0.0.1:9050" # IP:PORT or HOST:PORT
options = webdriver.ChromeOptions()
options.add_argument('--proxy-server=%s' % PROXY)
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)

conf = OmegaConf.load('conf.yml')

results_url = (f'{conf.webscraping.root_url}{conf.webscraping.results_table_url}'
               f'cod_primaria={conf.webscraping.cod_primaria}'
               f'&CodGrupo={conf.webscraping.CodGrupo}')


print('Charging web')
driver.get(results_url)
page = driver.page_source
results_page = BeautifulSoup(page, 'html.parser')

table_rows = results_page.find('table', {'class': 'table'}).findAll('tr')
table_results = table_rows[1:(len(table_rows))]

teams = []
for row in table_results:
    teams.append(row.find('td', {'colspan': '2'}).text.strip(' \t\r\n\xc2\xa0'))

results = []
row_idx = 0
for row in table_results:
    home_team = teams[row_idx]
    row_td = row.findAll('td')
    team_results = row_td[1:(len(row_td))]
    col_idx = 0
    for result in team_results:
        if '-' in result.text:
            away_team = teams[col_idx]
            results.append(
                {
                    'home_team': home_team,
                    'away_team': away_team,
                    'result': result.text.strip(' \t\r\n\xc2\xa0')
                }
            )
        col_idx += 1
    row_idx += 1

with open('results.csv', 'w', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=results[0].keys())
    writer.writeheader()
    writer.writerows(results)
